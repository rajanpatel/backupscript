**Without somebody to help me test this with windows 7 through 10, this is going to have to be classified as vaporware. i am happy to resume development if somebody is interested in testing. please email: rajanpatel@responsiveweb.com**

![BackupScript v1.5](http://storage.googleapis.com/backupscript/logo.png)

Google Cloud Storage *gsutil rsync* BackupScript v1.5
=====================================================

### Setup Steps

1.	[Install BackupScript](backupscript/wiki/setup/Install)
2.	[Get Google Cloud Storage](backupscript/wiki/setup/Cloud)

### Performing Backups

1.	[Backup Option 1](backupscript/wiki/backups/Option1)
2.	[Backup Option 2](backupscript/wiki/backups/Option2)
3.	[Scheduled Backups](backupscript/wiki/backups/Scheduled)
4.	[Choose What to Backup](backupscript/wiki/backups/Folders)
5.	[Synchronize Multiple Computers](backupscript/wiki/backups/Synchronize)

### Technical Information for Advanced Users

1.	[rsync: the BackupScript file synchronization protocol](backupscript/wiki/technical/rsync)
2.	[File Versioning](backupscript/wiki/technical/Versioning)

***

**About BackupScript**
======================

**BackupScript can synchronize large files (up to 5 terabytes per file) and folders up to 10s of terabytes in size over the Internet to the cloud, and synchronize User Profiles and additional folders across multiple computers, in a multi-processed and multi-threaded manner.**

### BackupScript backs up and synchronizes unlimited folders

BackupScript is coded specifically for the purpose of synchronizing files between 1 computer and the cloud, and multiple computers with each other by using the cloud as an intermediary. It is not a collaboration tool like Dropbox, Google Drive, or SkyDrive. BackupScript is exclusively a file synchronization and backup tool. The featureset and code optimizations in BackupScript are geared specifically for rapid file backup, synchronization, and versioning with Cloud Services that are designed for that purpose.

This software is not limited to 1 folder, like DropBox, SkyDrive, Google Drive, and others. Out of the box, BackupScript will backup and synchronize your Desktop, Documents, Pictures, Music, and Movies folders. You can add other folders to the list with ease. All of these folders and files can be synchronized between Mac OS X, Linux Operating Systems, and Windows based computers, without encountering complications introduced by different file systems across each operating system.

At the time of this writing, [Google Cloud Storage is the cheapest platform](https://cloud.google.com/pricing/cloud-storage) for both durable and reduced durability storage. BackupScript can be modified to leverage other more expensive cloud providers that support interactions through GSUTIL, such as Amazon S3, but out of the box it is designed to work exclusively with Google Cloud Storage.

### Supported Platforms 

Fully Tested Operating Systems:

1.	Windows Server 2003
2.	Windows 7
3.	Windows 8 and Windows 8.1

Untested Operating Systems (but still 100% supported)

1.	Windows Vista
2.	Windows Server 2008

There are plans to bring Linux and Mac OS X support to BackupScript in the very near future. Windows XP and Windows Server 2000 are not supported, and will never be supported.

### Support
Free support can be obtained by opening tickets in the [Issue Tracker](backupscript/issues). Paid telephone support can be obtained on an hourly basis at $125/hour, billed in 1 hour intervals, rounded up to the next whole hour. Remit payment for the first hour by sending $125 via PayPal to rajanpatel@responsiveweb.com and include your phone number with your payment.


### Security and Standards Compliance

The component pieces utilized for file transfer and the storage layer it interacts with are SOC2, SSAE 16 & ISAE 3402 compliant. Companies use the SOC2, SSAE 16 Type II audit, and its international counterpart ISAE 3402 Type II audit, (all formerly known as SAS 70) to document and verify the data protections in place for their services. 

In addition to passing those audits, the Google Cloud Storage layer has also achieved ISO 27001 compliance, which is one today's most widely recognized, internationally accepted independent security standards.

If you wish to use BackupScript for HIPAA compliant backups and file synchronization to Google Cloud Storage, ensure you are not exposing Personally Identifiable Information (PII) through the naming conventions of the files you are transferring.

The scripting portions of BackupScript do not introduce any vulnerabilities that would be a concern for compliance violations, but it is worth noting that BackupScript has not been independantly audited. Whenever compliance standards are a concern, it is strongly advised you [consult with Google directly](https://cloud.google.com/contact/) regarding your approach to ensure you are not delinquent of any legal standards.


Contribute Code and Suggest Improvements
========================================
My goal is to develop a backup solution which outperforms commercially available products for reliability and speed. To do this, I am focusing on a single cloud platform and write scripts using the most basic scripting platforms which are native to each Operating System.

If you are interested in writing bash scripts, applescripts, python scripts, or powershell scripts, this project has plenty of opportunities for you.

People that can refine this project's documentation, write how-to guides, and create screencast tutorials on YouTube would be excellent resources for this project, and I would love to hear from you.

If coding isn't your forte, your ongoing use of this software coupled with feedback for feature improvements and user experience enhancements will benefit this project, and everybody that uses BackupScript for their file syncrhonization and backup needs. 


Credits
-------
This software is distributed with Open Source contributions from the following:

`logo.ico` `logo.png` `logo.psd` are derivative works of an MIT licensed icon for [RaphaelJS](http://raphaeljs.com/icons/)
> Copyright © 2008 Dmitry Baranovskiy
 
`tail.exe` `libiconv2.dll` `libintl3.dll` are an MIT licensed component of [GNUWIN32 CoreUtils for Windows](http://gnuwin32.sourceforge.net/packages/coreutils.htm)
> Copyright © 2005 Free Software Foundation, Inc (Paul Rubin, David MacKenzie, Ian Lance Taylor, and Jim Meyering)

`BackupScript` at [code.rajanpatel.com](http://code.rajanpatel.com)
> Copyright © 2014 Rajan Patel

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. 

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.