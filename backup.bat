@echo off


TITLE Google Cloud Storage GSUTIL BackupScript v1.5
mkdir "%HomeDrive%%HomePath%\backupscript\logs" > NUL 2>&1
mkdir "%HomeDrive%%HomePath%\backupscript\uniqueid" > NUL 2>&1


:: Below is code for executedate and executetime for file naming
	For /f "tokens=2-4 delims=/ " %%a in ('date /t') do (set executedate=%%c-%%a-%%b)
	For /f "tokens=1-2 delims=/:" %%a in ("%TIME%") do (set executetime=%%a%%b)
	:: Remove space from time when it's before noon
	set executetime=%executetime: =%
:: Above is code for executedate and executetime for file naming

:: Files only created when needed, but this is what they are named and where they will live
set ProposedSyncFilename=%executedate%_%executetime%-proposedsync.txt
set ProposedSync=%HomeDrive%%HomePath%\backupscript\logs\%ProposedSyncFilename%
set ProposedOneWaySyncFilename=%executedate%_%executetime%-proposedonewaysync.txt
set ProposedOneWaySync=%HomeDrive%%HomePath%\backupscript\logs\%ProposedOneWaySyncFilename%
set CompletedSyncFilename=%executedate%_%executetime%-completedsync.txt
set CompletedSync=%HomeDrive%%HomePath%\backupscript\logs\%CompletedSyncFilename%


:: Process parameters passed to this batch file to determine storage availability and bucketinterval
:: http://stackoverflow.com/questions/14286457/using-parameters-in-batch-files-at-dos-command-line
set bucketinterval=1d
set backupmode=0
set DURABILITY=-c S
:parse
	IF "%~1"=="" GOTO endparse REM remember to change bucketinterval in line 16
	IF "%~1"=="-daily" set bucketinterval=1d
	IF "%~1"=="-weekly" set bucketinterval=7d
	IF "%~1"=="-monthly" set bucketinterval=1m
	IF "%~1"=="-yearly" set bucketinterval=1y

	IF "%~1"=="-backupoption1" set backupmode=1
	IF "%~1"=="-backupoption2" set backupmode=2
	
	IF "%~1"=="-DRA" set DURABILITY=-c DRA
	IF "%~1"=="-S" set DURABILITY=-c S
	SHIFT
	GOTO parse
:endparse

:: Find out 7 digit unique backup identifier
if exist "%HomeDrive%%HomePath%\backupscript\uniqueid\id.cfg" (
    set /p _RndAlphaNum=<"%HomeDrive%%HomePath%\backupscript\uniqueid\id.cfg"
) else (
	CALL :RANDOMSTRING
)

IF [%backupmode%]==[1] GOTO PERFORM1WAYSYNC
IF [%backupmode%]==[2] GOTO PERFORM2WAYSYNC

REM Below is code for colored text
SETLOCAL EnableDelayedExpansion
FOR /F "tokens=1,2 delims=#" %%a in ('"prompt #$H#$E# & echo on & for %%b in (1) do rem"') do (
  SET "DEL=%%a"
)
REM Above is code for colored text

:START
cls
CALL :ColorText 19 "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
CALL :ColorText 0C "                 Google Cloud Storage GSUTIL BackupScript v1.5"
ECHO.
CALL :ColorText 19 "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
ECHO.
ECHO.
CALL :ColorText 0a "  Backup Option 1 "
ECHO.
call :ColorText 0b "  PC files copy to cloud, files deleted or not on PC are DELETED in cloud."
ECHO.
ECHO.
CALL :ColorText 0a "  Backup Option 2 "
ECHO.
call :ColorText 0b "  PC files copy to cloud, files deleted or not on PC are RESTORED from cloud."
ECHO.
ECHO.
CHOICE /C 12Q /N /M "Choose your Backup Type:  Backup Option[1]  Backup Option[2]  [Q]uit: "
SET CHOICE=%ERRORLEVEL%
IF %CHOICE% EQU 1 GOTO 1WAYSYNC
IF %CHOICE% EQU 2 GOTO 2WAYSYNC
IF %CHOICE% EQU 3 GOTO END

:2WAYSYNC
cls
CALL :ColorText 19 "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
CALL :ColorText 0C "                 Google Cloud Storage GSUTIL BackupScript v1.5"
ECHO.
CALL :ColorText 19 "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
ECHO.
ECHO.
ECHO  Backup Option 2: files deleted or not on PC are restored from cloud.
ECHO.
CALL :ColorText 0a " 2 WAY SYNC OPTION IF FILES NEED TO BE RESTORED FROM CLOUD"
ECHO.
call :ColorText 0b " Locally Deleted Files will be Restored from the Cloud"
ECHO.
ECHO.
CHOICE /C YSCQ /N /M "Review Report for a 2 Way Sync?  [Y]es [S]kip Report & Sync [C]ancel [Q]uit: "
SET CHOICE=%ERRORLEVEL%
IF %CHOICE% EQU 1 GOTO 2WAYSYNCREPORTGENERATION
IF %CHOICE% EQU 2 GOTO PERFORM2WAYSYNC
IF %CHOICE% EQU 3 GOTO START
IF %CHOICE% EQU 4 GOTO END

:2WAYSYNCREPORTGENERATION
cls
CALL :ColorText 19 "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
CALL :ColorText 0C "                 Google Cloud Storage GSUTIL BackupScript v1.5"
ECHO.
CALL :ColorText 19 "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
ECHO.
ECHO.
ECHO  Backup Option 2: files deleted or not on PC are restored from cloud.
ECHO.
CALL :ColorText 0a " PLEASE WAIT"
call :ColorText 0b "  Generating list of files which will copy from and to the Cloud"
ECHO.
ECHO.
ECHO  Monitor Progress in realtime by watching this text file
ECHO  %ProposedSync%
ECHO.
ECHO.

set "SyncTypeLog=%ProposedSync%"
set rsyncparams=-n -c -C -r

:: start "" powershell.exe .\tail.bat %ProposedSyncFilename%
:: launching "%HomeDrive%%HomePath%\backupscript\bin\tail.exe" independently results in a race condition, report is ready before tail is done
start "Realtime Monitoring" "%HomeDrive%%HomePath%\backupscript\bin\tail.exe" -F "%SyncTypeLog%"

echo.> "%SyncTypeLog%"
echo.>> "%SyncTypeLog%"
echo ======================================================================= >> "%SyncTypeLog%"
ECHO Backup Option 2: files deleted or not on PC are restored from the cloud >> "%SyncTypeLog%"
echo ======================================================================= >> "%SyncTypeLog%"
echo.>> "%SyncTypeLog%"
echo.>> "%SyncTypeLog%"

:: First, backup default folders to cloud
CALL :LOCAL2CLOUD-DEFAULTFOLDERS

:: Second, backup extra folders from the config file to cloud
if exist "%HomeDrive%%HomePath%\backupscript\thingstobackup.cfg" (
    CALL :LOCAL2CLOUD-THINGSTOBACKUP
)

:: Third, copy default folders from cloud
CALL :CLOUD2LOCAL-DEFAULTFOLDERS

:: Fourth, copy content from extra folders in the config file from cloud to local
if exist "%HomeDrive%%HomePath%\backupscript\thingstobackup.cfg" (
    CALL :CLOUD2LOCAL-THINGSTOBACKUP
)

:: Lastly, produce helper text in console
echo. >> "%SyncTypeLog%" 2>&1
echo. >> "%SyncTypeLog%" 2>&1
echo. >> "%SyncTypeLog%" 2>&1
echo. >> "%SyncTypeLog%" 2>&1
echo. >> "%SyncTypeLog%" 2>&1
echo. >> "%SyncTypeLog%" 2>&1
echo. >> "%SyncTypeLog%" 2>&1
echo. >> "%SyncTypeLog%" 2>&1
echo ================================ >> "%SyncTypeLog%" 2>&1
echo YOU CAN SAFELY CLOSE THIS WINDOW >> "%SyncTypeLog%" 2>&1
echo NOTE: BackupScript will not >> "%SyncTypeLog%" 2>&1
echo       proceed until you close >> "%SyncTypeLog%" 2>&1
echo       the report which has >> "%SyncTypeLog%" 2>&1
echo       opened. >> "%SyncTypeLog%" 2>&1
choice /C X /T 3 /D X > nul
Taskkill /IM tail.exe /F

:2WAYSYNCREPORTGENERATED
cls
CALL :ColorText 19 "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
CALL :ColorText 0C "                 Google Cloud Storage GSUTIL BackupScript v1.5"
ECHO.
CALL :ColorText 19 "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
ECHO.
ECHO.
ECHO  Backup Option 2: files deleted or not on PC are restored from cloud.
ECHO.
call :ColorText 0b " Report of files scheduled to synchronize with Cloud generated"
ECHO.
call :ColorText 0b " Locally deleted files that are backed up on the cloud will be downloaded."
ECHO.
ECHO.
ECHO  Review the Report
ECHO  %ProposedSync%
ECHO.
ECHO.
ECHO  The Report has been opened for your convenience
ECHO  COMPLETELY CLOSE the Report to proceed...
ECHO.
"%ProposedSync%"

cls
CALL :ColorText 19 "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
CALL :ColorText 0C "                 Google Cloud Storage GSUTIL BackupScript v1.5"
ECHO.
CALL :ColorText 19 "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
ECHO.
ECHO.
ECHO  Backup Option 2: files deleted or not on PC are restored from cloud.
ECHO.
call :ColorText 0b " Do you want to copy from and to the Cloud as outlined in Report"
ECHO ?
ECHO.
ECHO  The Report you just reviewed is located at
ECHO  %ProposedSync%
ECHO.
ECHO.
CHOICE /C YNCQ /N /M "Perform Sync?  [Y]es [N]o, review report again [C]ancel [Q]uit: "
SET CHOICE=%ERRORLEVEL%
IF %CHOICE% EQU 1 GOTO PERFORM2WAYSYNC
IF %CHOICE% EQU 2 GOTO 2WAYSYNCREPORTGENERATED
IF %CHOICE% EQU 3 GOTO START
IF %CHOICE% EQU 4 GOTO END

:PERFORM2WAYSYNC
cls
CALL :ColorText 19 "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
CALL :ColorText 0C "                 Google Cloud Storage GSUTIL BackupScript v1.5"
ECHO.
CALL :ColorText 19 "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
ECHO.
ECHO.
ECHO  Backup Option 2: files deleted or not on PC are restored from cloud.
ECHO.
CALL :ColorText 0a " PLEASE WAIT"
call :ColorText 0b "  Files are copying from and to the Cloud"
ECHO.
ECHO.
set "SyncTypeLog=%CompletedSync%"
set rsyncparams=-c -C -r

:: start "" powershell.exe .\tail.bat %CompletedSyncFilename%
:: launching "%HomeDrive%%HomePath%\backupscript\bin\tail.exe" independently results in a race condition, report is ready before tail is done
start "Realtime Monitoring" "%HomeDrive%%HomePath%\backupscript\bin\tail.exe" -F "%SyncTypeLog%"

echo.> "%SyncTypeLog%"
echo.>> "%SyncTypeLog%"
echo ======================================================================= >> "%SyncTypeLog%"
ECHO Backup Option 2: files deleted or not on PC are restored from the cloud >> "%SyncTypeLog%"
echo ======================================================================= >> "%SyncTypeLog%"
echo.>> "%SyncTypeLog%"
echo.>> "%SyncTypeLog%"

:: First, backup default folders to cloud
CALL :LOCAL2CLOUD-DEFAULTFOLDERS

:: Second, backup extra folders from the config file to cloud
if exist "%HomeDrive%%HomePath%\backupscript\thingstobackup.cfg" (
    CALL :LOCAL2CLOUD-THINGSTOBACKUP
)

:: Third, copy default folders from cloud
CALL :CLOUD2LOCAL-DEFAULTFOLDERS

:: Fourth, copy content from extra folders in the config file from cloud to local
if exist "%HomeDrive%%HomePath%\backupscript\thingstobackup.cfg" (
    CALL :CLOUD2LOCAL-THINGSTOBACKUP
)

:: Lastly, produce helper text in console
echo. >> "%SyncTypeLog%" 2>&1
echo. >> "%SyncTypeLog%" 2>&1
echo. >> "%SyncTypeLog%" 2>&1
echo. >> "%SyncTypeLog%" 2>&1
echo. >> "%SyncTypeLog%" 2>&1
echo. >> "%SyncTypeLog%" 2>&1
echo. >> "%SyncTypeLog%" 2>&1
echo. >> "%SyncTypeLog%" 2>&1
echo ==================== >> "%SyncTypeLog%" 2>&1
echo BACKUP HAS COMPLETED >> "%SyncTypeLog%" 2>&1
echo YOU CAN CLOSE THIS >> "%SyncTypeLog%" 2>&1
echo WINDOW SAFELY >> "%SyncTypeLog%" 2>&1
echo ==================== >> "%SyncTypeLog%" 2>&1
echo FULL REPORT AT: >> "%SyncTypeLog%" 2>&1
echo %HomeDrive%%HomePath%\backupscript\logs\%CompletedSyncFilename% >> "%SyncTypeLog%" 2>&1
choice /C X /T 3 /D X > nul
Taskkill /IM tail.exe /F
GOTO END

:1WAYSYNC
cls
CALL :ColorText 19 "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
CALL :ColorText 0C "                 Google Cloud Storage GSUTIL BackupScript v1.5"
ECHO.
CALL :ColorText 19 "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
ECHO.
ECHO.
ECHO  Backup Option 1: files deleted or not on PC are deleted in cloud.
ECHO.
CALL :ColorText 0a " 1 WAY SYNC OPTION IF CLOUD FILES NEED DELETING"
ECHO.
call :ColorText 0b " Files deleted or not on PC will be Deleted in the Cloud"
ECHO.
ECHO.
CHOICE /C YSCQ /N /M "Review Report for a 1 Way Sync?  [Y]es [S]kip Report & Sync [C]ancel [Q]uit: "
SET CHOICE=%ERRORLEVEL%
IF %CHOICE% EQU 1 GOTO 1WAYSYNCREPORTGENERATION
IF %CHOICE% EQU 2 GOTO PERFORM1WAYSYNC
IF %CHOICE% EQU 3 GOTO START
IF %CHOICE% EQU 4 GOTO END

:1WAYSYNCREPORTGENERATION
cls
CALL :ColorText 19 "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
CALL :ColorText 0C "                 Google Cloud Storage GSUTIL BackupScript v1.5"
ECHO.
CALL :ColorText 19 "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
ECHO.
ECHO.
CALL :ColorText 0a " PLEASE WAIT"
ECHO.
call :ColorText 0b "  Generating report for a 1 Way Sync"
ECHO.
ECHO.
ECHO  Monitor Progress in realtime by watching
ECHO  %ProposedOneWaySync%
ECHO.
ECHO.

set "SyncTypeLog=%ProposedOneWaySync%"
set rsyncparams=-n -c -C -d -r

:: start "" powershell.exe .\tail.bat %ProposedOneWaySyncFilename%
:: launching "%HomeDrive%%HomePath%\backupscript\bin\tail.exe" independently results in a race condition, report is ready before tail is done
start "Realtime Monitoring" "%HomeDrive%%HomePath%\backupscript\bin\tail.exe" -F "%SyncTypeLog%"

echo.> "%SyncTypeLog%"
echo.>> "%SyncTypeLog%"
echo ============================================================= >> "%SyncTypeLog%"
ECHO Backup Option 1: files deleted or not on PC are deleted in the cloud >> "%SyncTypeLog%"
echo ============================================================= >> "%SyncTypeLog%"
echo.>> "%SyncTypeLog%"
echo.>> "%SyncTypeLog%"

:: First, backup default folders
CALL :LOCAL2CLOUD-DEFAULTFOLDERS

:: Second, backup extra folders from the config file
:EXTRAFOLDERS
if exist "%HomeDrive%%HomePath%\backupscript\thingstobackup.cfg" (
    CALL :LOCAL2CLOUD-THINGSTOBACKUP
) else (
	GOTO REPORT1WAYCLOSURETEXT
)
:: Third, produce helper text in console
GOTO REPORT1WAYCLOSURETEXT

:REPORT1WAYCLOSURETEXT
echo. >> "%SyncTypeLog%" 2>&1
echo. >> "%SyncTypeLog%" 2>&1
echo. >> "%SyncTypeLog%" 2>&1
echo. >> "%SyncTypeLog%" 2>&1
echo. >> "%SyncTypeLog%" 2>&1
echo. >> "%SyncTypeLog%" 2>&1
echo. >> "%SyncTypeLog%" 2>&1
echo. >> "%SyncTypeLog%" 2>&1
echo ================================ >> "%SyncTypeLog%" 2>&1
echo YOU CAN SAFELY CLOSE THIS WINDOW >> "%SyncTypeLog%" 2>&1
echo NOTE: BackupScript will not >> "%SyncTypeLog%" 2>&1
echo       proceed until you close >> "%SyncTypeLog%" 2>&1
echo       the report which has >> "%SyncTypeLog%" 2>&1
echo       opened. >> "%SyncTypeLog%" 2>&1
choice /C X /T 3 /D X > nul
Taskkill /IM tail.exe /F

:1WAYSYNCREPORTGENERATED
cls
CALL :ColorText 19 "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
CALL :ColorText 0C "                 Google Cloud Storage GSUTIL BackupScript v1.5"
ECHO.
CALL :ColorText 19 "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
ECHO.
ECHO.
CALL :ColorText 0C " WARNING "
ECHO.
call :ColorText 0b "  READ REPORT CAREFULLY AND LOOK OUT FOR PERMENANT DELETES IN THE CLOUD!"
ECHO.
ECHO.
ECHO  The Report is located at and has been opened for your convenience
ECHO  %ProposedOneWaySync%
ECHO.
ECHO.
ECHO  Review the Report
ECHO  COMPLETELY CLOSE the Report to proceed...
"%ProposedOneWaySync%"

cls
CALL :ColorText 19 "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
CALL :ColorText 0C "                 Google Cloud Storage GSUTIL BackupScript v1.5"
ECHO.
CALL :ColorText 19 "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
ECHO.
ECHO.
CALL :ColorText 0C " WARNING "
call :ColorText 0b "  LAST CHANCE before actions from the Report are taken"
ECHO.
ECHO.
ECHO  The Report is located at
ECHO  %ProposedOneWaySync%
ECHO.
ECHO.
CHOICE /C YNCQ /N /M "Perform Sync?  [Y]es [N]o, review report again [C]ancel [Q]uit: "
SET CHOICE=%ERRORLEVEL%
IF %CHOICE% EQU 1 GOTO PERFORM1WAYSYNC
IF %CHOICE% EQU 2 GOTO 1WAYSYNCREPORTGENERATED
IF %CHOICE% EQU 3 GOTO START
IF %CHOICE% EQU 4 GOTO END


:PERFORM1WAYSYNC
cls
CALL :ColorText 19 "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
CALL :ColorText 0C "                 Google Cloud Storage GSUTIL BackupScript v1.5"
ECHO.
CALL :ColorText 19 "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
ECHO.
ECHO.
CALL :ColorText 0a " PLEASE WAIT"
call :ColorText 0b "  Files are copying from and to the Cloud. Some Cloud Files may be Deleted."
ECHO.
ECHO.
set "SyncTypeLog=%CompletedSync%"
set rsyncparams=-c -C -d -r

:: start "" powershell.exe .\tail.bat %CompletedSyncFilename%
:: launching "%HomeDrive%%HomePath%\backupscript\bin\tail.exe" independently results in a race condition, report is ready before tail is done
start "Realtime Monitoring" "%HomeDrive%%HomePath%\backupscript\bin\tail.exe" -F "%SyncTypeLog%"

echo.> "%SyncTypeLog%"
echo.>> "%SyncTypeLog%"
echo ==================================================================== >> "%SyncTypeLog%"
ECHO Backup Option 1: files deleted or not on PC are deleted in the cloud >> "%SyncTypeLog%"
echo ==================================================================== >> "%SyncTypeLog%"
echo.>> "%SyncTypeLog%"
echo.>> "%SyncTypeLog%"

:: First, backup default folders
CALL :LOCAL2CLOUD-DEFAULTFOLDERS

:: Second, backup extra folders from the config file
if exist "%HomeDrive%%HomePath%\backupscript\thingstobackup.cfg" (
    CALL :LOCAL2CLOUD-THINGSTOBACKUP
) else (
	GOTO 1WAYSYNCACTIONCLOSURETEXT
)
:: Third, produce helper text in console
GOTO 1WAYSYNCACTIONCLOSURETEXT

:1WAYSYNCACTIONCLOSURETEXT
echo. >> "%SyncTypeLog%" 2>&1
echo. >> "%SyncTypeLog%" 2>&1
echo. >> "%SyncTypeLog%" 2>&1
echo. >> "%SyncTypeLog%" 2>&1
echo. >> "%SyncTypeLog%" 2>&1
echo. >> "%SyncTypeLog%" 2>&1
echo. >> "%SyncTypeLog%" 2>&1
echo. >> "%SyncTypeLog%" 2>&1
echo ==================== >> "%SyncTypeLog%" 2>&1
echo BACKUP HAS COMPLETED >> "%SyncTypeLog%" 2>&1
echo YOU CAN CLOSE THIS >> "%SyncTypeLog%" 2>&1
echo WINDOW SAFELY >> "%SyncTypeLog%" 2>&1
echo ==================== >> "%SyncTypeLog%" 2>&1
echo FULL REPORT AT: >> "%SyncTypeLog%" 2>&1
echo %HomeDrive%%HomePath%\backupscript\logs\%CompletedSyncFilename% >> "%SyncTypeLog%" 2>&1
choice /C X /T 3 /D X > nul
Taskkill /IM tail.exe /F
GOTO END


REM Below is code for colored text
:ColorText
	:: COLOR EXAMPLES BELOW
	:: call :ColorText 19 "blue"
	:: CALL :ColorText 0a "green"
	:: call :ColorText 0C "red"
	:: call :ColorText 0b "lavendar"
	:: call :ColorText 19 "blue"
	:: call :ColorText 2F "white"
	:: call :ColorText 4e "yellow"
	echo off
	<nul set /p ".=%DEL%" > "%~2"
	findstr /v /a:%1 /R "^$" "%~2" nul
	del "%~2" > nul 2>&1
GOTO:EOF
:: Above is code for colored text

:: Below is code for Uppercase Lowercase
:LCase
:UCase
	:: Converts to upper/lower case variable contents
	:: Syntax: CALL :UCase _VAR1 _VAR2
	:: Syntax: CALL :LCase _VAR1 _VAR2
	:: _VAR1 = Variable NAME whose VALUE is to be converted to upper/lower case
	:: _VAR2 = NAME of variable to hold the converted value
	:: Note: Use variable NAMES in the CALL, not values (pass "by reference")
	
	SET _UCase=A B C D E F G H I J K L M N O P Q R S T U V W X Y Z
	SET _LCase=a b c d e f g h i j k l m n o p q r s t u v w x y z
	SET _Lib_UCase_Tmp=!%1!
	IF /I "%0"==":UCase" SET _Abet=%_UCase%
	IF /I "%0"==":LCase" SET _Abet=%_LCase%
	FOR %%Z IN (%_Abet%) DO SET _Lib_UCase_Tmp=!_Lib_UCase_Tmp:%%Z=%%Z!
	SET %2=%_Lib_UCase_Tmp%
GOTO:EOF
REM Above is code for Uppercase Lowercase

:RANDOMSTRING
	:: seven alphanumeric lowercase characters gives us 36^7 = 79 billion combinations
	setlocal EnableDelayedExpansion
	Set _RNDLength=7
	Set _Alphanumeric=abcdefghijklmnopqrstuvwxyz0123456789
	Set _Str=%_Alphanumeric%987654321
	:_LenLoop
	IF NOT "%_Str:~18%"=="" SET _Str=%_Str:~9%& SET /A _Len+=9& GOTO :_LenLoop
	SET _tmp=%_Str:~9,1%
	SET /A _Len=_Len+_tmp
	Set _count=0
	SET _RndAlphaNum=
	:_loop
	Set /a _count+=1
	SET _RND=%Random%
	Set /A _RND=_RND%%%_Len%
	SET _RndAlphaNum=%_RndAlphaNum%!_Alphanumeric:~%_RND%,1!
	If !_count! lss %_RNDLength% goto _loop
	:: Echo Random string is %_RndAlphaNum% 
	echo %_RndAlphaNum%>"%HomeDrive%%HomePath%\backupscript\uniqueid\id.cfg"
	set _RndAlphaNum=%_RndAlphaNum%
	endlocal
GOTO:EOF

:STARTTIME
echo.>> "%SyncTypeLog%"
echo.>> "%SyncTypeLog%"
	set STARTTIME=%TIME%
    for /F "tokens=1-4 delims=:.," %%a in ("%STARTTIME%") do (
       set /A "start=(((%%a*60)+1%%b %% 100)*60+1%%c %% 100)*100+1%%d %% 100"
    )
GOTO:EOF
:ENDTIME
    set ENDTIME=%TIME%
    for /F "tokens=1-4 delims=:.," %%a in ("%ENDTIME%") do (
       set /A "end=(((%%a*60)+1%%b %% 100)*60+1%%c %% 100)*100+1%%d %% 100"
    )
    set /A elapsed=end-start
    :: Format the results for output
    set /A hh=elapsed/(60*60*100), rest=elapsed%%(60*60*100), mm=rest/(60*100), rest%%=60*100, ss=rest/100, cc=rest%%100
    if %hh% lss 10 set hh=0%hh%
    if %mm% lss 10 set mm=0%mm%
    if %ss% lss 10 set ss=0%ss%
    if %cc% lss 10 set cc=0%cc%
    set DURATION=%hh%:%mm%:%ss%.%cc%
    set CONSOLEDURATION=%hh% hours %mm% minutes %ss%.%cc% seconds
>> "%SyncTypeLog%"	  echo ------------------------------------
>> "%SyncTypeLog%"    echo Start    :  %STARTTIME%
>> "%SyncTypeLog%"    echo Finish   :  %ENDTIME%
>> "%SyncTypeLog%"    echo             -----------
>> "%SyncTypeLog%"    echo Duration :  %DURATION%   hh:mm:ss
echo             completed in %CONSOLEDURATION%
echo.
echo.>> "%SyncTypeLog%"
echo.>> "%SyncTypeLog%"
GOTO:EOF

:LOCAL2CLOUD-THINGSTOBACKUP
	setlocal enableextensions enabledelayedexpansion
	for /f "delims=" %%i in (%HomeDrive%%HomePath%\backupscript\thingstobackup.cfg) do (
		set "_output="
		set _input=%%i
		set fullpathdir=%%i
		CALL :LOCAL2CLOUD-BUCKETACTIONS
	)
	:endfor
	endlocal
GOTO:EOF

:CLOUD2LOCAL-THINGSTOBACKUP
	setlocal enableextensions enabledelayedexpansion
	for /f "delims=" %%i in (%HomeDrive%%HomePath%\backupscript\thingstobackup.cfg) do (
		set "_output="
		set _input=%%i
		set fullpathdir=%%i
		CALL :CLOUD2LOCAL-BUCKETACTIONS
	)
	:endfor
	endlocal
GOTO:EOF

:LOCAL2CLOUD-DEFAULTFOLDERS
>> "%SyncTypeLog%" echo.
CALL :STARTTIME
echo Processing: %HomeDrive%%HomePath%\Desktop\
>> "%SyncTypeLog%" echo FILES ON DESKTOP
>> "%SyncTypeLog%" echo ================
>> "%SyncTypeLog%" echo gsutil mb %DURABILITY% gs://%bucketinterval%-%_RndAlphaNum%-desktop >> "%SyncTypeLog%" 2>&1
gsutil mb %DURABILITY% gs://%bucketinterval%-%_RndAlphaNum%-desktop >> "%SyncTypeLog%" 2>&1
>> "%SyncTypeLog%" echo gsutil -m rsync %rsyncparams% %HomeDrive%%HomePath%\Desktop\ gs://%bucketinterval%-%_RndAlphaNum%-desktop >> "%SyncTypeLog%" 2>&1
gsutil -m rsync %rsyncparams% %HomeDrive%%HomePath%\Desktop\ gs://%bucketinterval%-%_RndAlphaNum%-desktop >> "%SyncTypeLog%" 2>&1
echo.>> "%SyncTypeLog%"
echo.>> "%SyncTypeLog%"
>> "%SyncTypeLog%" echo %HomeDrive%%HomePath%\Desktop\
>> "%SyncTypeLog%" echo gs://%bucketinterval%-%_RndAlphaNum%-desktop
CALL :ENDTIME

CALL :STARTTIME
echo Processing: %HomeDrive%%HomePath%\Documents\
>> "%SyncTypeLog%" echo FILES IN MY DOCUMENTS
>> "%SyncTypeLog%" echo =====================
>> "%SyncTypeLog%" echo gsutil mb %DURABILITY% gs://%bucketinterval%-%_RndAlphaNum%-documents >> "%SyncTypeLog%" 2>&1
gsutil mb %DURABILITY% gs://%bucketinterval%-%_RndAlphaNum%-documents >> "%SyncTypeLog%" 2>&1
>> "%SyncTypeLog%" echo gsutil -m rsync %rsyncparams% %HomeDrive%%HomePath%\Documents\ gs://%bucketinterval%-%_RndAlphaNum%-documents >> "%SyncTypeLog%" 2>&1
gsutil -m rsync %rsyncparams% %HomeDrive%%HomePath%\Documents\ gs://%bucketinterval%-%_RndAlphaNum%-documents >> "%SyncTypeLog%" 2>&1
echo.>> "%SyncTypeLog%"
echo.>> "%SyncTypeLog%"
>> "%SyncTypeLog%" echo %HomeDrive%%HomePath%\Documents\ >> "%SyncTypeLog%" 2>&1
>> "%SyncTypeLog%" echo gs://%bucketinterval%-%_RndAlphaNum%-documents
CALL :ENDTIME

CALL :STARTTIME
echo Processing: %HomeDrive%%HomePath%\Pictures\
>> "%SyncTypeLog%" echo FILES IN MY PICTURES
>> "%SyncTypeLog%" echo ====================
>> "%SyncTypeLog%" echo gsutil mb %DURABILITY% gs://%bucketinterval%-%_RndAlphaNum%-pictures >> "%SyncTypeLog%" 2>&1
gsutil mb %DURABILITY% gs://%bucketinterval%-%_RndAlphaNum%-pictures >> "%SyncTypeLog%" 2>&1
>> "%SyncTypeLog%" echo gsutil -m rsync %rsyncparams% %HomeDrive%%HomePath%\Pictures\ gs://%bucketinterval%-%_RndAlphaNum%-pictures >> "%SyncTypeLog%" 2>&1
gsutil -m rsync %rsyncparams% %HomeDrive%%HomePath%\Pictures\ gs://%bucketinterval%-%_RndAlphaNum%-pictures >> "%SyncTypeLog%" 2>&1
echo.>> "%SyncTypeLog%"
echo.>> "%SyncTypeLog%"
>> "%SyncTypeLog%" echo %HomeDrive%%HomePath%\Pictures\ >> "%SyncTypeLog%" 2>&1
>> "%SyncTypeLog%" echo gs://%bucketinterval%-%_RndAlphaNum%-pictures
CALL :ENDTIME

CALL :STARTTIME
echo Processing: %HomeDrive%%HomePath%\Videos\
>> "%SyncTypeLog%" echo FILES IN MY VIDEOS
>> "%SyncTypeLog%" echo ==================
>> "%SyncTypeLog%" echo gsutil mb %DURABILITY% gs://%bucketinterval%-%_RndAlphaNum%-videos >> "%SyncTypeLog%" 2>&1
gsutil mb %DURABILITY% gs://%bucketinterval%-%_RndAlphaNum%-videos >> "%SyncTypeLog%" 2>&1
>> "%SyncTypeLog%" echo gsutil -m rsync %rsyncparams% %HomeDrive%%HomePath%\Videos\ gs://%bucketinterval%-%_RndAlphaNum%-videos >> "%SyncTypeLog%" 2>&1
gsutil -m rsync %rsyncparams% %HomeDrive%%HomePath%\Videos\ gs://%bucketinterval%-%_RndAlphaNum%-videos >> "%SyncTypeLog%" 2>&1
echo.>> "%SyncTypeLog%"
echo.>> "%SyncTypeLog%"
>> "%SyncTypeLog%" echo %HomeDrive%%HomePath%\Videos\ >> "%SyncTypeLog%" 2>&1
>> "%SyncTypeLog%" echo gs://%bucketinterval%-%_RndAlphaNum%-videos
CALL :ENDTIME

CALL :STARTTIME
echo Processing: %HomeDrive%%HomePath%\Music\
>> "%SyncTypeLog%" echo FILES IN MY MUSIC
>> "%SyncTypeLog%" echo =================
>> "%SyncTypeLog%" echo gsutil mb %DURABILITY% gs://%bucketinterval%-%_RndAlphaNum%-music >> "%SyncTypeLog%" 2>&1
gsutil mb %DURABILITY% gs://%bucketinterval%-%_RndAlphaNum%-music >> "%SyncTypeLog%" 2>&1
>> "%SyncTypeLog%" echo gsutil -m rsync %rsyncparams% %HomeDrive%%HomePath%\Music\ gs://%bucketinterval%-%_RndAlphaNum%-music >> "%SyncTypeLog%" 2>&1
gsutil -m rsync %rsyncparams% %HomeDrive%%HomePath%\Music\ gs://%bucketinterval%-%_RndAlphaNum%-music >> "%SyncTypeLog%" 2>&1
echo.>> "%SyncTypeLog%"
echo.>> "%SyncTypeLog%"
>> "%SyncTypeLog%" echo %HomeDrive%%HomePath%\Music\ >> "%SyncTypeLog%" 2>&1
>> "%SyncTypeLog%" echo gs://%bucketinterval%-%_RndAlphaNum%-music
CALL :ENDTIME

:BACKUPSCRIPTBACKUP
echo Processing: %HomeDrive%%HomePath%\backupscript\
CALL :STARTTIME
>> "%SyncTypeLog%" echo BACKUPSCRIPT FILES
>> "%SyncTypeLog%" echo ==================
>> "%SyncTypeLog%" gsutil mb %DURABILITY% gs://%bucketinterval%-%_RndAlphaNum%-backupscript >> "%SyncTypeLog%" 2>&1
gsutil mb %DURABILITY% gs://%bucketinterval%-%_RndAlphaNum%-backupscript >> "%SyncTypeLog%" 2>&1
>> "%SyncTypeLog%" gsutil -m rsync %rsyncparams% %HomeDrive%%HomePath%\backupscript\ gs://%bucketinterval%-%_RndAlphaNum%-backupscript >> "%SyncTypeLog%" 2>&1
gsutil -m rsync %rsyncparams% %HomeDrive%%HomePath%\backupscript\ gs://%bucketinterval%-%_RndAlphaNum%-backupscript >> "%SyncTypeLog%" 2>&1
echo.>> "%SyncTypeLog%"
echo.>> "%SyncTypeLog%"
>> "%SyncTypeLog%" echo gs://%bucketinterval%-%_RndAlphaNum%-backupscript
CALL :ENDTIME

GOTO:EOF

:CLOUD2LOCAL-DEFAULTFOLDERS
>> "%SyncTypeLog%" echo.
CALL :STARTTIME
echo Processing: %HomeDrive%%HomePath%\Desktop\
>> "%SyncTypeLog%" echo FILES ON DESKTOP
>> "%SyncTypeLog%" echo ================
>> "%SyncTypeLog%" echo gsutil mb %DURABILITY% gs://%bucketinterval%-%_RndAlphaNum%-desktop >> "%SyncTypeLog%" 2>&1
gsutil mb %DURABILITY% gs://%bucketinterval%-%_RndAlphaNum%-desktop >> "%SyncTypeLog%" 2>&1
>> "%SyncTypeLog%" echo gsutil -m rsync %rsyncparams% %HomeDrive%%HomePath%\Desktop\ gs://%bucketinterval%-%_RndAlphaNum%-desktop >> "%SyncTypeLog%" 2>&1
gsutil -m rsync %rsyncparams% %HomeDrive%%HomePath%\Desktop\ gs://%bucketinterval%-%_RndAlphaNum%-desktop >> "%SyncTypeLog%" 2>&1
echo.>> "%SyncTypeLog%"
echo.>> "%SyncTypeLog%"
>> "%SyncTypeLog%" echo %HomeDrive%%HomePath%\Desktop\
>> "%SyncTypeLog%" echo gs://%bucketinterval%-%_RndAlphaNum%-desktop
CALL :ENDTIME

CALL :STARTTIME
echo Processing: %HomeDrive%%HomePath%/Documents/
>> "%SyncTypeLog%" echo FILES IN MY DOCUMENTS
>> "%SyncTypeLog%" echo =====================
>> "%SyncTypeLog%" echo gsutil mb %DURABILITY% gs://%bucketinterval%-%_RndAlphaNum%-documents >> "%SyncTypeLog%" 2>&1
gsutil mb %DURABILITY% gs://%bucketinterval%-%_RndAlphaNum%-documents >> "%SyncTypeLog%" 2>&1
>> "%SyncTypeLog%" echo gsutil -m rsync %rsyncparams% %HomeDrive%%HomePath%\Documents\ gs://%bucketinterval%-%_RndAlphaNum%-documents >> "%SyncTypeLog%" 2>&1
gsutil -m rsync %rsyncparams% %HomeDrive%%HomePath%\Documents\ gs://%bucketinterval%-%_RndAlphaNum%-documents >> "%SyncTypeLog%" 2>&1
echo.>> "%SyncTypeLog%"
echo.>> "%SyncTypeLog%"
>> "%SyncTypeLog%" echo %HomeDrive%%HomePath%\Documents\ >> "%SyncTypeLog%" 2>&1
>> "%SyncTypeLog%" echo gs://%bucketinterval%-%_RndAlphaNum%-documents
CALL :ENDTIME

CALL :STARTTIME
echo Processing: %HomeDrive%%HomePath%\Pictures\
>> "%SyncTypeLog%" echo FILES IN MY PICTURES
>> "%SyncTypeLog%" echo ====================
>> "%SyncTypeLog%" echo gsutil mb %DURABILITY% gs://%bucketinterval%-%_RndAlphaNum%-pictures >> "%SyncTypeLog%" 2>&1
gsutil mb %DURABILITY% gs://%bucketinterval%-%_RndAlphaNum%-pictures >> "%SyncTypeLog%" 2>&1
>> "%SyncTypeLog%" echo gsutil -m rsync %rsyncparams% %HomeDrive%%HomePath%\Pictures\ gs://%bucketinterval%-%_RndAlphaNum%-pictures >> "%SyncTypeLog%" 2>&1
gsutil -m rsync %rsyncparams% %HomeDrive%%HomePath%\Pictures\ gs://%bucketinterval%-%_RndAlphaNum%-pictures >> "%SyncTypeLog%" 2>&1
echo.>> "%SyncTypeLog%"
echo.>> "%SyncTypeLog%"
>> "%SyncTypeLog%" echo %HomeDrive%%HomePath%\Pictures\ >> "%SyncTypeLog%" 2>&1
>> "%SyncTypeLog%" echo gs://%bucketinterval%-%_RndAlphaNum%-pictures
CALL :ENDTIME

CALL :STARTTIME
echo Processing: %HomeDrive%%HomePath%\Videos\
>> "%SyncTypeLog%" echo FILES IN MY VIDEOS
>> "%SyncTypeLog%" echo ==================
>> "%SyncTypeLog%" echo gsutil mb %DURABILITY% gs://%bucketinterval%-%_RndAlphaNum%-videos >> "%SyncTypeLog%" 2>&1
gsutil mb %DURABILITY% gs://%bucketinterval%-%_RndAlphaNum%-videos >> "%SyncTypeLog%" 2>&1
>> "%SyncTypeLog%" gsutil -m rsync %rsyncparams% %HomeDrive%%HomePath%\Videos\ gs://%bucketinterval%-%_RndAlphaNum%-videos >> "%SyncTypeLog%" 2>&1
gsutil -m rsync %rsyncparams% %HomeDrive%%HomePath%\Videos\ gs://%bucketinterval%-%_RndAlphaNum%-videos >> "%SyncTypeLog%" 2>&1
echo.>> "%SyncTypeLog%"
echo.>> "%SyncTypeLog%"
>> "%SyncTypeLog%" echo %HomeDrive%%HomePath%\Videos\ >> "%SyncTypeLog%" 2>&1
>> "%SyncTypeLog%" echo gs://%bucketinterval%-%_RndAlphaNum%-videos
CALL :ENDTIME

CALL :STARTTIME
echo Processing: %HomeDrive%%HomePath%\Music\
>> "%SyncTypeLog%" echo FILES IN MY MUSIC
>> "%SyncTypeLog%" echo =================
>> "%SyncTypeLog%" echo gsutil mb %DURABILITY% gs://%bucketinterval%-%_RndAlphaNum%-music >> "%SyncTypeLog%" 2>&1
gsutil mb %DURABILITY% gs://%bucketinterval%-%_RndAlphaNum%-music >> "%SyncTypeLog%" 2>&1
>> "%SyncTypeLog%" gsutil -m rsync %rsyncparams% %HomeDrive%%HomePath%\Music\ gs://%bucketinterval%-%_RndAlphaNum%-music >> "%SyncTypeLog%" 2>&1
gsutil -m rsync %rsyncparams% %HomeDrive%%HomePath%\Music\ gs://%bucketinterval%-%_RndAlphaNum%-music >> "%SyncTypeLog%" 2>&1
echo.>> "%SyncTypeLog%"
echo.>> "%SyncTypeLog%"
>> "%SyncTypeLog%" echo %HomeDrive%%HomePath%\Music\ >> "%SyncTypeLog%" 2>&1
>> "%SyncTypeLog%" echo gs://%bucketinterval%-%_RndAlphaNum%-music
CALL :ENDTIME

:BACKUPSCRIPTBACKUP
echo Processing: %HomeDrive%%HomePath%\backupscript\
CALL :STARTTIME
>> "%SyncTypeLog%" echo BACKUPSCRIPT FILES
>> "%SyncTypeLog%" echo ==================
>> "%SyncTypeLog%" gsutil mb %DURABILITY% gs://%bucketinterval%-%_RndAlphaNum%-backupscript >> "%SyncTypeLog%" 2>&1
gsutil mb %DURABILITY% gs://%bucketinterval%-%_RndAlphaNum%-backupscript >> "%SyncTypeLog%" 2>&1
>> "%SyncTypeLog%" echo gsutil -m rsync %rsyncparams% %HomeDrive%%HomePath%\backupscript\ gs://%bucketinterval%-%_RndAlphaNum%-backupscript >> "%SyncTypeLog%" 2>&1
gsutil -m rsync %rsyncparams% %HomeDrive%%HomePath%\backupscript\ gs://%bucketinterval%-%_RndAlphaNum%-backupscript >> "%SyncTypeLog%" 2>&1
echo.>> "%SyncTypeLog%"
echo.>> "%SyncTypeLog%"
>> "%SyncTypeLog%" echo gs://%bucketinterval%-%_RndAlphaNum%-backupscript
CALL :ENDTIME

GOTO:EOF

:LOCAL2CLOUD-BUCKETACTIONS
	CALL :STARTTIME
	:loop
	    if not defined _input goto endLoop
	    set "_buf=!_input:~0,1!"
	    set "_input=!_input:~1!"
	    echo "!_buf!"|findstr /i /r /c:"[a-z0-9]" > nul && set "_output=!_output!!_buf!"
	    goto loop
	:endLoop
	CALL :LCase _output _sanitizedlongbucketname
	set _googlestoragebucketname=%_sanitizedlongbucketname:~-25%
	echo Processing: %fullpathdir%
	>> "%SyncTypeLog%" echo FILES IN %fullpathdir%
	>> "%SyncTypeLog%" echo ================================================
	>> "%SyncTypeLog%" echo gsutil mb %DURABILITY% gs://%bucketinterval%-%_RndAlphaNum%-%_googlestoragebucketname% >> "%SyncTypeLog%" 2>&1
	gsutil mb %DURABILITY% gs://%bucketinterval%-%_RndAlphaNum%-%_googlestoragebucketname% >> "%SyncTypeLog%" 2>&1
	>> "%SyncTypeLog%" echo gsutil -m rsync %rsyncparams% "%fullpathdir%" gs://%bucketinterval%-%_RndAlphaNum%-%_googlestoragebucketname% >> "%SyncTypeLog%" 2>&1
	gsutil -m rsync %rsyncparams% "%fullpathdir%" gs://%bucketinterval%-%_RndAlphaNum%-%_googlestoragebucketname% >> "%SyncTypeLog%" 2>&1
	echo.>> "%SyncTypeLog%"
	echo.>> "%SyncTypeLog%"
	>> "%SyncTypeLog%" echo %fullpathdir%
	>> "%SyncTypeLog%" echo gs://%bucketinterval%-%_RndAlphaNum%-%_googlestoragebucketname%
	CALL :ENDTIME
GOTO:EOF

:CLOUD2LOCAL-BUCKETACTIONS
	CALL :STARTTIME
	:loop
	    if not defined _input goto endLoop
	    set "_buf=!_input:~0,1!"
	    set "_input=!_input:~1!"
	    echo "!_buf!"|findstr /i /r /c:"[a-z0-9]" > nul && set "_output=!_output!!_buf!"
	    goto loop
	:endLoop
	CALL :LCase _output _sanitizedlongbucketname
	set _googlestoragebucketname=%_sanitizedlongbucketname:~-25%
	echo Processing: %fullpathdir%
	>> "%SyncTypeLog%" echo FILES IN %fullpathdir%
	>> "%SyncTypeLog%" echo ================================================
	>> "%SyncTypeLog%" echo gsutil mb %DURABILITY% gs://%bucketinterval%-%_RndAlphaNum%-%_googlestoragebucketname% >> "%SyncTypeLog%" 2>&1
	gsutil mb %DURABILITY% gs://%bucketinterval%-%_RndAlphaNum%-%_googlestoragebucketname% >> "%SyncTypeLog%" 2>&1	
	>> "%SyncTypeLog%" echo gsutil -m rsync %rsyncparams% "%fullpathdir%" gs://%bucketinterval%-%_RndAlphaNum%-%_googlestoragebucketname% >> "%SyncTypeLog%" 2>&1
	gsutil -m rsync %rsyncparams% "%fullpathdir%" gs://%bucketinterval%-%_RndAlphaNum%-%_googlestoragebucketname% >> "%SyncTypeLog%" 2>&1
	echo.>> "%SyncTypeLog%"
	echo.>> "%SyncTypeLog%"
	>> "%SyncTypeLog%" echo %fullpathdir%
	>> "%SyncTypeLog%" echo gs://%bucketinterval%-%_RndAlphaNum%-%_googlestoragebucketname%
	CALL :ENDTIME
GOTO:EOF

:END
:BACKUPCOMPLETE
cls
CALL :ColorText 19 "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
CALL :ColorText 0C "                 Google Cloud Storage GSUTIL BackupScript v1.5"
ECHO.
CALL :ColorText 19 "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
ECHO.
ECHO.
CALL :ColorText 0a "  This window is safe to close. Press X to close this window."
choice /C X /T 10 /D X > nul
ENDLOCAL
::exit