<!-- : Begin INSTALL.BAT version 1.0
@echo off
REM Below is code for colored text
SETLOCAL EnableDelayedExpansion
FOR /F "tokens=1,2 delims=#" %%a in ('"prompt #$H#$E# & echo on & for %%b in (1) do rem"') do (
  SET "DEL=%%a"
)
REM Above is code for colored text

cls
echo ===============================================================================
echo Please be Patient                                      Do not close this window
echo ===============================================================================
echo.
echo   Installing the latest Google Cloud Storage GSUTIL BackupScript
echo.
echo      CODE.RAJANPATEL.COM
echo.
echo       distributed under the MIT License
echo       http://opensource.org/licenses/MIT
echo.
echo.

:: create destination directory and move files there
mkdir %HomeDrive%%HomePath%\backupscript >nul 2>&1
mkdir %HomeDrive%%HomePath%\backupscript\bin >nul 2>&1

GOTO GSUTILINSTALL
:: VBSCRIPT DOWNLOADING HAS CACHING ISSUES, USE GSUTIL INSTEAD
:: BEGIN VBSCRIPTED DOWNLOAD
CALL :ColorText 0a "  + DOWNLOADING FILES "
:: download the files
cscript //nologo "%~f0?.wsf" //job:download
echo    [ COMPLETE ]
CALL :ColorText 0a "  + INSTALLING "
move /Y backup.bat "%HomeDrive%%HomePath%\backupscript\bin" >nul 2>&1
move /Y logo.ico "%HomeDrive%%HomePath%\backupscript\bin" >nul 2>&1
move /Y tail.exe "%HomeDrive%%HomePath%\backupscript\bin" >nul 2>&1
move /Y libintl3.dll "%HomeDrive%%HomePath%\backupscript\bin" >nul 2>&1
move /Y libiconv2.dll "%HomeDrive%%HomePath%\backupscript\bin" >nul 2>&1
echo           [ COMPLETE ]
:: END VBSCRIPTED DOWNLOAD

:GSUTILINSTALL
mkdir %HomeDrive%%HomePath%\backupscript\
mkdir %HomeDrive%%HomePath%\backupscript\uniqueid
mkdir %HomeDrive%%HomePath%\backupscript\bin
del /F /Q %HomeDrive%%HomePath%\backupscript\bin\*.*
:: gsutil -m rsync -c -C -d -r gs://backupscript/ %HomeDrive%%HomePath%\backupscript\bin\
gsutil cp gs://backupscript/backup.bat %HomeDrive%%HomePath%\backupscript\bin\
gsutil cp gs://backupscript/logo.ico %HomeDrive%%HomePath%\backupscript\bin\
gsutil cp gs://backupscript/tail.exe %HomeDrive%%HomePath%\backupscript\bin\
gsutil cp gs://backupscript/libintl3.dll %HomeDrive%%HomePath%\backupscript\bin\
gsutil cp gs://backupscript/libiconv2.dll %HomeDrive%%HomePath%\backupscript\bin\

:: Find out 7 digit unique backup identifier
if exist "%HomeDrive%%HomePath%\backupscript\uniqueid\id.cfg" (
  set /p _RndAlphaNum=<"%HomeDrive%%HomePath%\backupscript\uniqueid\id.cfg"
  choice /d y /t 5 > nul
  echo.
  echo Your unique id is:
  type "%HomeDrive%%HomePath%\backupscript\uniqueid\id.cfg"
  echo.
  echo    It is set in: %HomeDrive%%HomePath%\backupscript\uniqueid\id.cfg
) else (
  CALL :RANDOMSTRING
  echo.
  echo Your unique id is:
  type "%HomeDrive%%HomePath%\backupscript\uniqueid\id.cfg"
  echo.
  echo    It has been set in: %HomeDrive%%HomePath%\backupscript\uniqueid\id.cfg  
)

:: check if a config file exists, create it if it does not
if exist "%HomeDrive%%HomePath%\backupscript\thingstobackup.cfg" (
	echo.
	echo.
	echo   You configured additional Directories to be backed up in:
	echo    %HomeDrive%%HomePath%\backupscript\thingstobackup.cfg
	echo.
	echo   Review this file for correctness.
	echo.
	echo.
) else (
    echo. 2>%HomeDrive%%HomePath%\backupscript\thingstobackup.cfg
)


:: BEGIN create shortcut on desktop
SETLOCAL ENABLEDELAYEDEXPANSION
SET LinkName=BackupScript
SET Esc_LinkDest=%%HOMEDRIVE%%%%HOMEPATH%%\Desktop\!LinkName!.lnk
SET Esc_LinkTarget=%%HOMEDRIVE%%%%HOMEPATH%%\backupscript\bin\backup.bat
SET Esc_LinkIcon=%%HOMEDRIVE%%%%HOMEPATH%%\backupscript\bin\logo.ico
SET cSctVBS=CreateShortcut.vbs
SET LOG=".\%~N0_delete_me_runtime.log"
((
  echo Set oWS = WScript.CreateObject^("WScript.Shell"^) 
  echo sLinkFile = oWS.ExpandEnvironmentStrings^("!Esc_LinkDest!"^)
  echo Set oLink = oWS.CreateShortcut^(sLinkFile^) 
  echo oLink.TargetPath = oWS.ExpandEnvironmentStrings^("!Esc_LinkTarget!"^)
  echo oLink.IconLocation = oWS.ExpandEnvironmentStrings^("!Esc_LinkIcon!"^)
  echo oLink.Save
)1>!cSctVBS!
cscript //nologo .\!cSctVBS!
DEL !cSctVBS! /f /q
)1>>!LOG! 2>>&1

:: END of create shortcut on desktop
echo ===============================================================================
echo INSTALLATION COMPLETE              There is a BackupScript icon on your Desktop
echo ===============================================================================
del install_delete_me_runtime.log >nul 2>&1
CHOICE /C YNCQ /N /M "Close this Window?  [Y]es"
IF %CHOICE% EQU 1 GOTO:EOF
exit /b

:RANDOMSTRING
  :: seven alphanumeric lowercase characters gives us 36^7 = 79 billion combinations
  setlocal EnableDelayedExpansion
  Set _RNDLength=7
  Set _Alphanumeric=abcdefghijklmnopqrstuvwxyz0123456789
  Set _Str=%_Alphanumeric%987654321
  :_LenLoop
  IF NOT "%_Str:~18%"=="" SET _Str=%_Str:~9%& SET /A _Len+=9& GOTO :_LenLoop
  SET _tmp=%_Str:~9,1%
  SET /A _Len=_Len+_tmp
  Set _count=0
  SET _RndAlphaNum=
  :_loop
  Set /a _count+=1
  SET _RND=%Random%
  Set /A _RND=_RND%%%_Len%
  SET _RndAlphaNum=!_RndAlphaNum!!_Alphanumeric:~%_RND%,1!
  If !_count! lss %_RNDLength% goto _loop
  :: Echo Random string is !_RndAlphaNum! 
  echo !_RndAlphaNum!>"%HomeDrive%%HomePath%\backupscript\uniqueid\id.cfg"
  choice /d y /t 5 > nul
  set _RndAlphaNum=!_RndAlphaNum!
  endlocal
GOTO:EOF

REM Below is code for colored text
:ColorText
	:: COLOR EXAMPLES BELOW
	:: call :ColorText 19 "blue"
	:: CALL :ColorText 0a "green"
	:: call :ColorText 0C "red"
	:: call :ColorText 0b "lavendar"
	:: call :ColorText 19 "blue"
	:: call :ColorText 2F "white"
	:: call :ColorText 4e "yellow"
	echo off
	<nul set /p ".=%DEL%" > "%~2"
	findstr /v /a:%1 /R "^$" "%~2" nul
	del "%~2" > nul 2>&1
GOTO:EOF
:: Above is code for colored text


----- Begin wsf script --->
<package>
  <job id="download">
    <script language="VBScript">
dim xHttpFile: Set xHttpFile = createobject("Microsoft.XMLHTTP")
dim bStrmFile: Set bStrmFile = createobject("Adodb.Stream")
xHttpFile.Open "GET", "http://storage.googleapis.com/backupscript/backup.bat", False
xHttpFile.Send

with bStrmFile
    .type = 1 '//binary
    .open
    .write xHttpFile.responseBody
    .savetofile "backup.bat", 2 '//overwrite
end with

dim xHttpFirstFile: Set xHttpFirstFile = createobject("Microsoft.XMLHTTP")
dim bStrmFirstFile: Set bStrmFirstFile = createobject("Adodb.Stream")
xHttpFirstFile.Open "GET", "http://storage.googleapis.com/backupscript/logo.ico", False
xHttpFirstFile.Send

with bStrmFirstFile
    .type = 1 '//binary
    .open
    .write xHttpFirstFile.responseBody
    .savetofile "logo.ico", 2 '//overwrite
end with

dim xHttpSecondFile: Set xHttpSecondFile = createobject("Microsoft.XMLHTTP")
dim bStrmSecondFile: Set bStrmSecondFile = createobject("Adodb.Stream")
xHttpSecondFile.Open "GET", "http://storage.googleapis.com/backupscript/tail.exe", False
xHttpSecondFile.Send

with bStrmSecondFile
    .type = 1 '//binary
    .open
    .write xHttpSecondFile.responseBody
    .savetofile "tail.exe", 2 '//overwrite
end with

dim xHttpThirdFile: Set xHttpThirdFile = createobject("Microsoft.XMLHTTP")
dim bStrmThirdFile: Set bStrmThirdFile = createobject("Adodb.Stream")
xHttpThirdFile.Open "GET", "http://storage.googleapis.com/backupscript/libintl3.dll", False
xHttpThirdFile.Send

with bStrmThirdFile
    .type = 1 '//binary
    .open
    .write xHttpThirdFile.responseBody
    .savetofile "libintl3.dll", 2 '//overwrite
end with

dim xHttpFourthFile: Set xHttpFourthFile = createobject("Microsoft.XMLHTTP")
dim bStrmFourthFile: Set bStrmFourthFile = createobject("Adodb.Stream")
xHttpFourthFile.Open "GET", "http://storage.googleapis.com/backupscript/libiconv2.dll", False
xHttpFourthFile.Send

with bStrmFourthFile
    .type = 1 '//binary
    .open
    .write xHttpFourthFile.responseBody
    .savetofile "libiconv2.dll", 2 '//overwrite
end with
    </script>
  </job>
</package>