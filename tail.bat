@echo off
:: tail -n 10 should outperform Get-Content, but our GNU tail suffers the same whole file problem as Get-Content (and behaves worse!)
:: Get-Content reads the whole file with each iteration and develops race condition problems as well
:: while the tail.bat approach for refreshing the last 10 lines is deprecated in favor of GNU tail.exe -F, I am leaving it here for reference
:: if somebody wants to build upon this further, or for whatever reason wishes not to use MSFT Server Admin Tools tail.exe or GNU tail.exe

:watchprogress
cls
powershell Get-Content "%HomeDrive%%HomePath%\backupscript\logs\%~1" -Last 10
echo.
echo    This Window can be closed
echo ================================
choice /C X /T 1 /D X > nul
goto watchprogress